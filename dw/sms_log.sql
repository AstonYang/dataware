create EXTERNAL table if not exists dw.sms_log(
    msisdn       string    COMMENT '号码',
    smslabel        string    COMMENT '签名',
    msg_content string comment '内容' ,
    cust_id int comment '客户id',
    insert_time timestamp,
    msg_report string,
    status int,
    provider_id int ,
    city string,
    prov_id int,
    area string
 )
partitioned by (pt string)
stored as orc tblproperties ("orc.compress"="SNAPPY");

alter table dw.sms_log add  if not exists partition(pt='201606');
alter table dw.sms_log add  if not exists partition(pt='201607');
alter table dw.sms_log add  if not exists partition(pt='201608');
alter table dw.sms_log add  if not exists partition(pt='201609');
alter table dw.sms_log add  if not exists partition(pt='201610');
alter table dw.sms_log add  if not exists partition(pt='201611');
alter table dw.sms_log add  if not exists partition(pt='201612');
alter table dw.sms_log add  if not exists partition(pt='201701');
alter table dw.sms_log add  if not exists partition(pt='201702');
alter table dw.sms_log add  if not exists partition(pt='201703');
alter table dw.sms_log add  if not exists partition(pt='201704');
alter table dw.sms_log add  if not exists partition(pt='201705');
alter table dw.sms_log add  if not exists partition(pt='201706');
alter table dw.sms_log add  if not exists partition(pt='201707');
alter table dw.sms_log add  if not exists partition(pt='201708');
alter table dw.sms_log add  if not exists partition(pt='201709');
alter table dw.sms_log add  if not exists partition(pt='201710');
alter table dw.sms_log add  if not exists partition(pt='201711');
alter table dw.sms_log add  if not exists partition(pt='201712');
alter table dw.sms_log add  if not exists partition(pt='201801');
alter table dw.sms_log add  if not exists partition(pt='201802');
alter table dw.sms_log add  if not exists partition(pt='201803');
alter table dw.sms_log add  if not exists partition(pt='201804');
alter table dw.sms_log add  if not exists partition(pt='201805');
alter table dw.sms_log add  if not exists partition(pt='201806');


alter table history.games_zcx add  if not exists partition(pt='20171029');
alter table history.games_zcx add  if not exists partition(pt='20171101');
alter table history.games_zcx add  if not exists partition(pt='20171102');
alter table history.games_zcx add  if not exists partition(pt='20171104');
alter table history.games_zcx add  if not exists partition(pt='20171105');
alter table history.games_zcx add  if not exists partition(pt='20171106');
alter table history.games_zcx add  if not exists partition(pt='20171108');
alter table history.games_zcx add  if not exists partition(pt='20171109');
alter table history.games_zcx add  if not exists partition(pt='20171110');
alter table history.games_zcx add  if not exists partition(pt='20171111');





433.0 K  866.0 K   /user/hive/warehouse/history.db/games_zcx/pt=20171029
4.7 M    9.5 M     /user/hive/warehouse/history.db/games_zcx/pt=20171101
555.8 K  1.1 M     /user/hive/warehouse/history.db/games_zcx/pt=20171102
95.0 K   189.9 K   /user/hive/warehouse/history.db/games_zcx/pt=20171104
2.6 M    5.2 M     /user/hive/warehouse/history.db/games_zcx/pt=20171105
1.6 M    3.3 M     /user/hive/warehouse/history.db/games_zcx/pt=20171106
577.0 K  1.1 M     /user/hive/warehouse/history.db/games_zcx/pt=20171108
843.4 K  1.6 M     /user/hive/warehouse/history.db/games_zcx/pt=20171109
509.7 K  1019.5 K  /user/hive/warehouse/history.db/games_zcx/pt=20171110
17.1 M   290.3 M   /user/hive/warehouse/history.db/games_zcx/pt=20171111


create EXTERNAL table if not exists dw.1 (msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.2 (msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.3 (msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.4 (msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.5 (msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.6 (msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.7 (msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.8 (msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.9 (msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.10(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.11(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.12(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.13(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.14(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.15(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.16(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.17(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.18(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.19(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");
create EXTERNAL table if not exists dw.20(msisdn string COMMENT '号码') stored as orc tblproperties ("orc.compress"="SNAPPY");


alter table dw.f_1  set location '/user/hive/warehouse/dw.db/1';
alter table dw.f_2  set location '/user/hive/warehouse/dw.db/2';
alter table dw.f_3  set location '/user/hive/warehouse/dw.db/3';
alter table dw.f_4  set location '/user/hive/warehouse/dw.db/4';
alter table dw.f_5  set location '/user/hive/warehouse/dw.db/5';
alter table dw.f_6  set location '/user/hive/warehouse/dw.db/6';
alter table dw.f_7  set location '/user/hive/warehouse/dw.db/7';
alter table dw.f_8  set location '/user/hive/warehouse/dw.db/8';
alter table dw.f_9  set location '/user/hive/warehouse/dw.db/9';
alter table dw.f_10 set location '/user/hive/warehouse/dw.db/10';
alter table dw.f_11 set location '/user/hive/warehouse/dw.db/11';
alter table dw.f_12 set location '/user/hive/warehouse/dw.db/12';
alter table dw.f_13 set location '/user/hive/warehouse/dw.db/13';
alter table dw.f_14 set location '/user/hive/warehouse/dw.db/14';
alter table dw.f_15 set location '/user/hive/warehouse/dw.db/15';
alter table dw.f_16 set location '/user/hive/warehouse/dw.db/16';
alter table dw.f_17 set location '/user/hive/warehouse/dw.db/17';
alter table dw.f_18 set location '/user/hive/warehouse/dw.db/18';
alter table dw.f_19 set location '/user/hive/warehouse/dw.db/19';
alter table dw.f_20 set location '/user/hive/warehouse/dw.db/20';
alter table dw.f_21 set location '/user/hive/warehouse/dw.db/21';