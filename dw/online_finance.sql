create EXTERNAL table if not exists dw.online_finance(
    msisdn       string    COMMENT '号码',
    smslabel        string    COMMENT '签名',
    msg_content string comment '内容' ,
    cust_id int comment '客户id',
    insert_time timestamp,
    msg_report string,
    status int,
    provider_id int ,
    city string,
    prov_id int,
    area string
 )
partitioned by (pt string)
stored as orc tblproperties ("orc.compress"="SNAPPY");