# coding: UTF-8
###借款成功，下款的消息
from pyspark import SparkContext,SparkConf
from pyspark.sql import HiveContext
import sys, datetime, time,json
import urllib
reload(sys)
sys.setdefaultencoding('utf-8')
conf = SparkConf().set("spark.sql.parquet.compression.codec", "snappy")
sc = SparkContext("yarn-client", "online_finance", conf=conf)
sqlContext = HiveContext(sc)


def parse_data(pt):
df=sqlContext.sql("""

    select
        *
    FROM
        dw.temp_qiche
    WHERE
        smslabel LIKE '%奔驰%'
        OR smslabel LIKE '%森那美%'
        OR smslabel LIKE '%南京利之星%'
        OR smslabel LIKE '%爱卡%'
        OR smslabel LIKE '%腾讯汽车%'
        OR smslabel LIKE '%易车生活%'
        OR smslabel LIKE '%搜狐汽车%'
        OR smslabel LIKE '%东风日产%'
        OR smslabel LIKE '%丰田%'
        limit 5000
""")

df=sqlContext.sql("""
    CREATE TABLE dw.temp_baoxian as
    select
        *
    FROM
        dw.sms_log
    WHERE
        msg_content LIKE '%平安保险%'
        OR msg_content LIKE '%太平人寿%'
        OR msg_content LIKE '%中国平安%'
        OR msg_content LIKE '%新华保险%'
        OR msg_content LIKE '%人寿保险%'
        OR msg_content LIKE '%太平洋保险%'
        OR msg_content LIKE '%中国人民保险%'
        OR msg_content LIKE '%华夏保险%'
        OR msg_content LIKE '%大都会保险%'
        OR msg_content LIKE '%太平保险%'
""")
    hdfs_path = """pt=%s""" % pt

    df.repartition(10).write.mode("overwrite").orc("/user/hive/warehouse/dw.db/online_finance/%s" % hdfs_path)
if __name__ == "__main__":
    print time.time()
    pt = sys.argv[1]
    df = parse_data(pt)
