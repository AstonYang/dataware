
create EXTERNAL table if not exists history.beauty(
    msisdn       string    COMMENT '号码',
    city string
 ) comment '美容提取记录'
partitioned by (pt string)
stored as orc tblproperties ("orc.compress"="SNAPPY");