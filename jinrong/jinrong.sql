create EXTERNAL table if not exists jinrong.zhengquan(
    msisdn       string    COMMENT '号码',
    smslabel        string    COMMENT '签名',
    msg_content string comment '内容' ,
    cust_id int comment '客户id',
    insert_time timestamp,
    msg_report string,
    status int,
    provider_id int ,
    city string,
    prov_id int,
    area string
 )
partitioned by (pt string)
stored as orc tblproperties ("orc.compress"="SNAPPY");

alter table jinrong.zhengquan add  if not exists partition(pt='20181218');


        msisdn,
        smslabel,
        msg_content,
        cust_id,
        insert_time,
        msg_report,
        status ,
        provider_id  ,
        city ,
        prov_id ,
        area