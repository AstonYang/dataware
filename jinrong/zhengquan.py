

# 数据挖掘
df=sqlContext.sql("""

    select
        msisdn,
        smslabel,
        msg_content,
        cust_id,
        insert_time,
        msg_report,
        status ,
        provider_id  ,
        city ,
        prov_id ,
        area
    FROM
        dw.sms_log
    WHERE
        msg_content LIKE '%理财%'
        OR msg_content LIKE '%收益%'
        OR msg_content LIKE '%回报%'
        OR msg_content LIKE '%比特%'
        OR msg_content LIKE '%交易%'
""")
df.repartition(1).write.mode("overwrite").orc("/user/hive/warehouse/jinrong.db/zhengquan/pt=20190218/" )

