
create EXTERNAL table if not exists ganhuo.ganhuo_20181108(
    msisdn       string    COMMENT '号码',
    smslabel        string    COMMENT '签名',
    msg_content string comment '内容' ,
    cust_id int comment '客户id',
    insert_time timestamp,
    msg_report string,
    status int,
    provider_id int ,
    city string,
    prov_id int,
    area string
 ) comment '借款成功，下款的消息'
partitioned by (pt string)
row format delimited
fields terminated by ','